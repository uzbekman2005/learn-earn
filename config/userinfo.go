package config

import (
	"database/sql"
	"fmt"

	"github.com/go-redis/redis"
	_ "github.com/lib/pq"
)

type User struct {
	First_name  string
	Last_name   string
	Username    string
	Password    string
	Score       int
	HighScore   int
	Country     string
	DateOfBirth string
}


var CurrentUser User
var SecondUser User

func CreatConnetion() *sql.DB {
	cURL := `host=localhost port=5432 user=azizbek password=Azizbek dbname=shopping sslmode=disable`
	DB, err := sql.Open("postgres", cURL)
	if err != nil {
		fmt.Println(err)
	}
	return DB
}

func Client() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
}
