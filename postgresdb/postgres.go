package postgresdb

import (
	"time"
)

type Customer struct {
	ID            int
	FirstName     string
	LastName      string
	Password      string
	Username      string
	Date_of_birth time.Time
	CreditCard    CreditInfo
}

type Store struct {
	ID      int
	Name    string
	Address string
	Budget  float64
}

type Porduct struct {
	ID            int
	Name          string
	TypeID        int
	CategoryID    int
	MeasureUnitID int
	Price         float64
}

type Type struct {
	ID   int
	Name string
}

type Category struct {
	ID   int
	Name string
}

type MeasureUnit struct {
	ID   int
	Name string
}

type CreditInfo struct {
	CardName   string
	CardNumber string
	Password   string
	Balance    float64
}
