module learn

go 1.19

require (
	github.com/bxcodec/faker/v4 v4.0.0-beta.2
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/lib/pq v1.10.7
)

require (
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.20.2 // indirect
	golang.org/x/text v0.3.7 // indirect
)
