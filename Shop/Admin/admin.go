package admin

import (
	"database/sql"
	"fmt"
	"learn/globalfunctions"
	"learn/postgresdb"
)

func AddminMain(db *sql.DB) {
	n := globalfunctions.InputNum("Number of products: ")
	for i := 1; i <= n; i++ {
		globalfunctions.SystemClear()
		err := CreateNewProduct(db)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}

func CreateNewProduct(db *sql.DB) error {
	ShowTCM(db)
	product := postgresdb.Porduct{}
	product.Name = globalfunctions.InputString("Name: ")
	product.TypeID = globalfunctions.InputNum("Type id: ")
	product.CategoryID = globalfunctions.InputNum("Category id: ")
	product.MeasureUnitID = globalfunctions.InputNum("Measure Unit_id: ")
	product.Price = globalfunctions.InputFloat("Price: ")
	_, err := db.Exec(`INSERT INTO products(
		name, 
		type_id, 
		category_id, 
		measure_unit_id,
		price) 
		VALUES($1, $2, $3, $4, $5);`, product.Name, product.TypeID, product.CategoryID, product.MeasureUnitID, product.Price)
	return err
}

func ShowTCM(db *sql.DB) {
	types, err := GetTypes(db)
	globalfunctions.CheckErr(err)
	categories, err := GetCategories(db)
	globalfunctions.CheckErr(err)
	units, err := GetMeasureUnits(db)
	globalfunctions.CheckErr(err)
	fmt.Println("+--------------------+--------------------+--------------------+")
	fmt.Println("|      Types         |      Categories    |    Units           |")
	fmt.Println("|--------------------+--------------------+--------------------|")
	fmt.Println("| ID |    Name       | ID |     Name      | ID |   Name        |")
	fmt.Println("|----+---------------+----+---------------+----+---------------|")
	for t, c, u := 0, 0, 0; t < len(types) || c < len(categories) || u < len(units); {
		tp := postgresdb.Type{}
		ct := postgresdb.Category{}
		un := postgresdb.MeasureUnit{}
		if t < len(types) {
			tp.ID = types[t].ID
			tp.Name = types[t].Name
			t++
		}
		if c < len(categories) {
			ct.ID = categories[c].ID
			ct.Name = categories[c].Name
			c++
		}
		if u < len(units) {
			un.ID = units[u].ID
			un.Name = units[u].Name
			u++
		}
		fmt.Printf("| %-3d| %-14s| %-3d| %-14s| %-3d| %-14s|\n", tp.ID, tp.Name,
			ct.ID, ct.Name, un.ID, un.Name)
		fmt.Println("|----+---------------+----+---------------+----+---------------|")
	}
}

func GetTypes(db *sql.DB) ([]postgresdb.Type, error) {
	rows, err := db.Query(`select type_id, name from types;`)
	globalfunctions.CheckErr(err)
	res := []postgresdb.Type{}
	for rows.Next() {
		temp := postgresdb.Type{}
		err = rows.Scan(&temp.ID, &temp.Name)
		globalfunctions.CheckErr(err)
		res = append(res, temp)
	}
	rows.Close()
	return res, err
}

func GetCategories(db *sql.DB) ([]postgresdb.Category, error) {
	rows, err := db.Query(`select category_id, name from categories;`)
	globalfunctions.CheckErr(err)
	res := []postgresdb.Category{}
	for rows.Next() {
		temp := postgresdb.Category{}
		err = rows.Scan(&temp.ID, &temp.Name)
		globalfunctions.CheckErr(err)
		res = append(res, temp)
	}
	rows.Close()
	return res, err
}

func GetMeasureUnits(db *sql.DB) ([]postgresdb.MeasureUnit, error) {
	rows, err := db.Query(`select measure_unit_id, name from measure_units;`)
	globalfunctions.CheckErr(err)
	res := []postgresdb.MeasureUnit{}
	for rows.Next() {
		temp := postgresdb.MeasureUnit{}
		err = rows.Scan(&temp.ID, &temp.Name)
		globalfunctions.CheckErr(err)
		res = append(res, temp)
	}

	return res, err
}
