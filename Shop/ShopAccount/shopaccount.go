package shopaccount

import (
	"database/sql"
	"fmt"
	"learn/config"
	"learn/globalfunctions"
	"learn/postgresdb"
	"time"

	faker "github.com/bxcodec/faker/v4"
)

var err error

func OpenShopAccount(db *sql.DB) *postgresdb.Customer {
	shopAccount := &postgresdb.Customer{}
	shopAccount.FirstName = config.CurrentUser.First_name
	shopAccount.LastName = config.CurrentUser.Last_name
	shopAccount.Date_of_birth, err = time.Parse(`2006-01-02`, config.CurrentUser.DateOfBirth)
	if err != nil {
		panic(err)
	}
	shopAccount.Password = config.CurrentUser.Password
	shopAccount.Username = config.CurrentUser.Username
	err = OpenCreditCard(shopAccount, db)
	if err != nil {
		panic(err)
	}
	globalfunctions.SystemClear()
	fmt.Println("Succes")
	return shopAccount
}

func OpenCreditCard(sha *postgresdb.Customer, db *sql.DB) error {
	ShowCreditCards(db)
	card_id := globalfunctions.InputNum("Enter id  of choosed Card: ")
	err := db.QueryRow(`select name from credit_card where credit_card_id=$1`, card_id).Scan(&sha.CreditCard.CardName)
	if err != nil {
		fmt.Println(err)
	}
	sha.CreditCard.CardNumber = faker.CreditCardNumber
	err = db.QueryRow(`insert into card_info(credit_card_number, card_id, balance) 
	values($1, $2, $3) returning balance`, sha.CreditCard.CardNumber, card_id, 1000.0).Scan(&sha.CreditCard.Balance)
	return err
}

func ShowCreditCards(db *sql.DB) {
	rows, err := db.Query(`select credit_card_id, name from credit_card`)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var id int
	var name string
	fmt.Println("+----+---------------------------+")
	fmt.Println("| ID |     Name                  |")
	fmt.Println("+----+---------------------------+")
	for rows.Next() {
		err := rows.Scan(&id, &name)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("| %-3d| %-28s\n", id, name)
		fmt.Println("+----+---------------------------+")
	}
}
