package shopowner

import (
	"database/sql"
	"fmt"
	"learn/postgresdb"
)

func ShowAllShops(db *sql.DB, owner_id int) {
	store := postgresdb.Store{}
	rows, err := db.Query(`select 
		store_id,
		name,
		address,
		from stores where owner_id=$1
	`, owner_id)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	fmt.Println("+-----+------------------+----------------------------+")
	fmt.Println("| ID  |      Name        |          Address           |")
	fmt.Println("+-----+------------------+----------------------------+")
	for rows.Next() {
		err := rows.Scan(&store.ID, &store.Name, &store.Address)
		if err != nil {
			panic(err)
		}
		fmt.Printf("| %-4d| %-17s| %-27s|\n", store.ID, store.Name, store.Address)
		fmt.Println("+-----+------------------+----------------------------+")
	}
}

