package shopowner

import (
	"database/sql"
	"fmt"
	"learn/globalfunctions"
	"learn/postgresdb"
)

func ShopOwnerMain(owner *postgresdb.Customer, db *sql.DB) {
	for {
		ShopOwnerMenu()
		choose := globalfunctions.InputNum(">>> ")
		if choose == 1 {
			ShowAllShops(db, owner.ID)
		} else if choose == 2 {
			// 
		} else {
			break
		}

	}
}

func ShopOwnerMenu() {
	fmt.Println("  1  -> YouShops")
	fmt.Println("  2  -> Add product to store (only for admins)")
	fmt.Println("  3  -> Exit")
}
