package shopfunctions

import (
	"database/sql"
	"fmt"
	"learn/globalfunctions"
	"learn/postgresdb"
)

type ShowedProduct struct {
	ID          int
	Name        string
	Type        postgresdb.Type
	Category    postgresdb.Category
	MeasureUnit postgresdb.MeasureUnit
	Price       float64
}

func OpenNewStore(db *sql.DB) error {
	store := postgresdb.Store{}
	store.Name = globalfunctions.InputString("Store name: ")
	store.Address = globalfunctions.InputString("Enter Address: ")
	store.Budget = float64(globalfunctions.InputNum("Budget"))
	_, err := db.Query(`INSERT INTO stores(name, address, budget)
		VALUES($1 $2 $3)`, store.Name, store.Address, store.Budget)
	return err
}

func AddnewProductToStore(db *sql.DB, owner postgresdb.Customer) {
	var store_id int
	for {
		store_id = globalfunctions.InputNum("Enter store_id: ")

		err := db.QueryRow(`select name from stores where owner_id=$1 and store_id=$2`,
			owner.ID, store_id).Err()
		if err == nil {
			break
		}
		fmt.Println("Enter Right store id")
	}
	for {
		fmt.Println(" 1. Add Product")
		fmt.Println(" 2. Exit")
		choose := globalfunctions.InputNum(">>>")
		if choose == 2 {
			return
		}
		product_id := globalfunctions.InputNum("Enter product ID: ")
		amount := globalfunctions.InputFloat("Enter amount: ")
		tr, err := db.Begin()
		globalfunctions.CheckErr(err)
		_, err = tr.Exec(`update customers 
		credit=credit- (
			(select price from products where product_id = $1) * $2 
		)
		where customer_id=$3`, product_id, amount, owner.ID)
		if err != nil {
			tr.Rollback()
			fmt.Println("You don't have enough money to get that")
		}
		globalfunctions.CheckErr(err)
		_, err = tr.Exec(`
			insert into products_in_store values($1 $2 $3)
		`, store_id, product_id, amount)
		if err != nil {
			tr.Rollback()
			fmt.Println("Error while inserting into products_in_store table")
		}
		err = tr.Commit()
		if err != nil {
			fmt.Print("Error while commting")
			panic(err)
		}
	}
}

func ShowAllProducts(db *sql.DB) {
	rows, err := db.Query(`select 
		product_id,
		name, 
		t.name,
		c.name,
		m.name,
		price 
		from products p 
		join types t using(type_id) 
		join categories c ON c.category_id = p.category_id 
		join measure_units m ON m.measure_unit_id = p.measure_unit_id
		`)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	fmt.Println("+----+---------------+--------------+---------------+-------+-------+")
	fmt.Println("| ID |  Name         |  Type        |  Category     |  Unit | Price |")
	fmt.Println("+----+---------------+--------------+---------------+-------+-------+")

	for rows.Next() {
		product := ShowedProduct{}
		err := rows.Scan(&product.ID, &product.Name, &product.Type.Name,
			&product.Category.Name, product.MeasureUnit.Name, product.Price)
		globalfunctions.CheckErr(err)
		fmt.Printf("| %-3d| %-14s| %-13s| %-14s| %-6s| %-4.2f", product.ID, product.Name, product.Type.Name, product.Category.Name, product.MeasureUnit.Name, product.Price)
		fmt.Println("+----+---------------+--------------+---------------+-------+-------+")
	}
}
